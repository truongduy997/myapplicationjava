import Manager.Create.CreateStudents;
import Manager.Create.CreateTeachers;
import java.util.Scanner;

public class MainApplication {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("1.Quản lí giáo viên.");
        System.out.println("2.Quản lí sinh viên.");
        System.out.println("Nhập tác vụ bạn muốn:");

        int choose = scanner.nextInt();

        switch (choose) {
            case 1:
                CreateTeachers teachers = new CreateTeachers();
                teachers.createTeacher();
                break;
            case 2:
                CreateStudents students = new CreateStudents();
                students.createStudent();
                break;
            default:
                System.out.println("Nhập sai tác vụ.");
                break;
        }
    }
}