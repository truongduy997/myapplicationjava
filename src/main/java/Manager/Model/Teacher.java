package Manager.Model;

import java.util.Scanner;

public class Teacher extends Person {

    private String teachingClass;
    private String teacherID;
    private String teacherEmail;
    private int salary;


    @Override
    public void inputInfomationPerson() {
        super.inputInfomationPerson();
    }

    @Override
    public void getInfomationPerson() {
        super.getInfomationPerson();
    }

    public Teacher() {
    }

    public void inputinfomationTeacher(){
        this.inputInfomationPerson();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhập lớp đang dạy:");
        this.setTeachingClass(scanner.nextLine());

        System.out.println("Nhập mã giáo viên:");
        while (!this.setTeacherID(scanner.nextLine()));

        System.out.println("Nhập Email:");
        while (!this.setTeacherEmail(scanner.nextLine()));

        System.out.println("Nhập tiền lương giáo viên:");
        while ((!this.setSalary(scanner.nextInt())));

    }
    public void getInfomationTeacher(){
        this.getInfomationPerson();

        System.out.println("Lớp đang dạy: " + this.getTeachingClass());
        System.out.println("Mã giáo viên: " + this.getTeacherID());
        System.out.println("Email: " + this.getTeacherEmail());
        System.out.println("<------------------------------------------------->");
    }

    public int getSalary() {
        return salary;
    }

    public boolean setSalary(int salary) {
        int maxSalary = 20000000;
        if(salary > maxSalary) {
            System.out.println("Số tiền không vượt quá 20 triệu đồng.");
            return false;
        }
        this.salary = salary;
        return true;
    }

    public String getTeachingClass() {
        return teachingClass;
    }

    public void setTeachingClass(String teachingClass) {
        this.teachingClass = teachingClass;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public boolean setTeacherID(String teacherID) {
        if(teacherID.length() == 5) {
            this.teacherID = teacherID;
            return true;
        }
        System.err.println("Mã giáo viên phải là 5 ký tự.");
        return false;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public boolean setTeacherEmail(String teacherEmail) {
        if (teacherEmail != null && teacherEmail.contains("@") && !teacherEmail.contains(" ")){
            this.teacherEmail = teacherEmail;
            return true;
        }
        System.err.println("Email chưa đúng định dạng, xin nhập lại: ");
        return false;
    }

}
