package Manager.Model;

import java.util.Scanner;

public class Student extends Person {

    private String studentEmail;
    private String studentClass;
    private String studentID;
    private float studentPoint;

    @Override
    public void inputInfomationPerson() {
        super.inputInfomationPerson();
    }

    @Override
    public void getInfomationPerson() {
        super.getInfomationPerson();
    }


    public void inputInfomationStudent() {
        this.inputInfomationPerson();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhập lớp học: ");
        this.setStudentClass(scanner.nextLine());

        System.out.println("Nhập mã học sinh: ");
        while (!this.setStudentID(scanner.nextLine())) ;

        System.out.println("Nhập Email: ");
        while (!this.setStudentEmail(scanner.nextLine())) ;

        System.out.println("Nhập điểm trung bình: ");
        while (!this.setStudentPoint(scanner.nextFloat())) ;

        System.out.println("Thêm sinh viên thành công.");
    }

    public void getInfomationStudent() {
        this.getInfomationPerson();
        System.out.println("Lớp học: " + this.getStudentClass());
        System.out.println("Mã học sinh: " + this.getStudentID());
        System.out.println("Email: " + this.getStudentEmail());
        System.out.println("Điểm trung bình: " + this.getStudentPoint());
        System.out.println("<------------------------------------------------->");
    }

    public float getStudentPoint() {
        return studentPoint;
    }

    public boolean setStudentPoint(float studentPoint) {
        if (studentPoint > 0 && studentPoint <= 10) {
            this.studentPoint = studentPoint;
            return true;
        } else {
            System.err.println("Điểm trung bình phải lớn hơn 0 và nhỏ hơn 10: ");
            return false;
        }
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public boolean setStudentEmail(String studentEmail) {
        if (studentEmail != null && studentEmail.contains("@") && !studentEmail.contains(" ")) {
            this.studentEmail = studentEmail;
            return true;
        }
        System.err.println("Email chưa đúng định dạng, xin nhập lại: ");
        return false;
    }

    public Student() {
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public String getStudentID() {
        return studentID;
    }

    public boolean setStudentID(String studentID) {
        if (studentID.length() == 5) {
            this.studentID = studentID;
            return true;
        }
        System.err.println("Mã sinh viên phải là 5 ký tự.");
        return false;
    }
}
