package Manager.Model;

import java.util.Scanner;

public abstract class Person {
    private String name;
    private String gender;
    private int age;
    private String birthday;
    private String address;

    private Scanner scanner = new Scanner(System.in);

    public Person() {
    }

    public void inputInfomationPerson() {

        System.out.println("Nhập tên: ");
        this.setName(scanner.nextLine());

        System.out.println("Nhập giới tính: ");
        this.setGender(scanner.nextLine());

        System.out.println("Nhập tuổi: ");
        while (!this.setAge(Integer.parseInt(scanner.nextLine())));

        System.out.println("Nhập ngày sinh: ");
        this.setBirthday(scanner.nextLine());

        System.out.println("Nhập địa chỉ: ");
        this.setAddress(scanner.nextLine());
    }

    public void getInfomationPerson(){
        System.out.println("<------------------------------------------------->");
        System.out.println("Tên: " + this.getName());
        System.out.println("Giới tính: " + this.getGender());
        System.out.println("Tuổi: " + this.getAge());
        System.out.println("Ngày sinh: " + this.getBirthday());
        System.out.println("Địa chỉ: " + this.getAddress());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public boolean setAge(int age) {
        if (age <= 10){
            System.err.println("Nhập lại tuổi (tuổi phải lớn hơn 10): ");
            return false;
        }
        this.age = age;
        return true;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
