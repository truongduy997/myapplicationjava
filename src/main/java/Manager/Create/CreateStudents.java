package Manager.Create;

import Manager.Model.Student;

import java.util.ArrayList;
import java.util.Scanner;

public class CreateStudents {
    private ArrayList<Student> studentArrayList = new ArrayList<Student>();
    private Scanner scanner = new Scanner(System.in);
    // Menu
    private void showMenuStudent() {
        System.out.println("<------------------------------------------------->");
        System.out.println("Menu:");
        System.out.println("1. Tạo sinh viên, nhập số lượng.");
        System.out.println("2. Hiển thị tất cả sinh viên.");
        System.out.println("3. Sinh viên có điểm trung bình cao nhất và thấp nhất.");
        System.out.println("4. Tìm kiếm theo mã sinh viên.");
        System.out.println("5. Thoát.");
        System.out.println("<------------------------------------------------->");
    }

    // tạo sinh viên
    public void createStudent() {
        int choose;
        do {
            this.showMenuStudent();
            System.out.println("Chọn tác vụ bạn muốn: ");
            choose = scanner.nextInt();
            switch (choose) {
                //
                case 1:
                    System.out.println("Nhập số sinh viên muốn thêm: ");
                    int n = scanner.nextInt();
                    for (int i = 0; i < n; i++) {
                        Student student = new Student();
                        student.inputInfomationStudent();
                        studentArrayList.add(student);
                    }
                    System.out.println("Đã thêm thành công " + n + " sinh viên.");
                    break;
                //
                case 2:
                    if (studentArrayList.size() > 0) {
                        studentArrayList.forEach((item) -> {
                            item.getInfomationStudent();
                        });
                    } else {
                        System.out.println("Danh sách sinh viên trống.");
                    }
                    break;
                //
                case 3:
                    int minIndex = 0, maxIndex = 0;
                    float minPoint = studentArrayList.get(0).getStudentPoint();
                    float maxPoint = studentArrayList.get(0).getStudentPoint();
                    for (int i = 1; i < studentArrayList.size(); i++) {
                        if (studentArrayList.get(i).getStudentPoint() < minPoint) {
                            minIndex = i;
                            minPoint = studentArrayList.get(i).getStudentPoint();
                        }
                        if (studentArrayList.get(i).getStudentPoint() > maxPoint) {
                            maxIndex = i;
                            maxPoint = studentArrayList.get(i).getStudentPoint();
                        }
                    }
                    System.out.println("Sinh viên có điểm trung bình thấp nhất: ");
                    studentArrayList.get(minIndex).getInfomationStudent();
                    System.out.println("Sinh viên có điểm trung bình cao nhất: ");
                    studentArrayList.get(maxIndex).getInfomationStudent();
                    break;
                //
                case 4:
                    System.out.println("Nhập mã sinh viên:");
                    String searchString = scanner.next();
                    for (Student student : studentArrayList) {
                        if (student.getStudentID().equals(searchString)) {
                            System.out.println("Kết quả tìm kiếm:");
                            student.getInfomationStudent();
                        } else {
                            System.out.println("Không tìm thấy sinh viên có mã bạn đã nhập.");
                        }
                    }
                    break;
                case 5:
                    System.out.println("Đã thoát thành công.");
                    break;
                default:
                    System.out.println("Nhập sai tác vụ.");
                    this.createStudent();
                    break;
            }
        } while (choose != 5);
    }

}
