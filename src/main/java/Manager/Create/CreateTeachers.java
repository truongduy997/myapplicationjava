package Manager.Create;

import Manager.Model.Teacher;

import java.util.ArrayList;
import java.util.Scanner;

public class CreateTeachers {
    private ArrayList<Teacher> teacherArrayList = new ArrayList<Teacher>();
    private Scanner scanner = new Scanner(System.in);

    // Menu
    private void showMenuTeacher() {
        System.out.println("<------------------------------------------------->");
        System.out.println("Menu:");
        System.out.println("1. Tạo giáo viên, nhập số lượng.");
        System.out.println("2. Hiển thị tất cả giáo viên.");
        System.out.println("3. Giáo viên có tiền lương cao nhất và thấp nhất.");
        System.out.println("4. Tìm kiếm theo mã giáo viên.");
        System.out.println("5. Thoát.");
        System.out.println("<------------------------------------------------->");
    }


    public void createTeacher(){
        int choose;
        do {
            this.showMenuTeacher();
            System.out.println("Chọn tác vụ bán muốn: ");
            choose = scanner.nextInt();
            switch (choose) {
                //
                case 1:
                    System.out.println("Nhập số giáo viên muốn thêm: ");
                    int n = scanner.nextInt();
                    for (int i = 0; i < n; i++) {
                        Teacher teacher = new Teacher();
                        teacher.inputinfomationTeacher();
                        teacherArrayList.add(teacher);
                    }
                    System.out.println("Đã thêm thành công " + n + " giáo viên.");
                    break;
                //
                case 2:
                    if (teacherArrayList.size() > 0) {
                        for (Teacher item : teacherArrayList) {
                            item.getInfomationTeacher();
                        }
                    } else {
                        System.err.println("Danh sách giáo viên trống.");
                    }
                    break;
                //
                case 3:
                    int minIndex = 0, maxIndex = 0;
                    float minSalary = teacherArrayList.get(0).getSalary();
                    float maxSalary = teacherArrayList.get(0).getSalary();
                    for (int i = 1; i < teacherArrayList.size(); i++) {
                        if (teacherArrayList.get(i).getSalary() < minSalary) {
                            minIndex = i;
                            minSalary = teacherArrayList.get(i).getSalary();
                        }
                        if (teacherArrayList.get(i).getSalary() > maxSalary) {
                            maxIndex = i;
                            maxSalary = teacherArrayList.get(i).getSalary();
                        }
                    }
                    System.out.println("Giáo viên có tiền lương thấp nhất: ");
                    teacherArrayList.get(minIndex).getInfomationTeacher();
                    System.out.println("Giáo viên có tiền lương cao nhất: ");
                    teacherArrayList.get(maxIndex).getInfomationTeacher();
                    break;
                case 4:
                    System.out.println("Nhập mã giáo viên:");
                    String searchString = scanner.next();
                    for (Teacher student : teacherArrayList) {
                        if (student.getTeacherID().equals(searchString)) {
                            System.out.println("Kết quả tìm kiếm:");
                            student.getInfomationTeacher();
                        } else {
                            System.out.println("Không tìm thấy giáo viên có mã bạn đã nhập.");
                        }
                    }
                    break;
                case 5:
                    System.out.println("Đã thoát thành công.");
                    break;
                default:
                    System.out.println("Nhập sai tác vụ.");
                    this.createTeacher();
                    break;
            }
        } while (choose != 5);
    }

}

